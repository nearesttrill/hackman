package main

import (
	"fmt"
	_ "project/hackman/routers"
	"github.com/astaxie/beego"
	_ "github.com/go-sql-driver/mysql"
	_ "database/sql"
	"github.com/astaxie/beego/orm"
)

func init() {
	
	name := "default"
	force := false
	verbose := true

    orm.RegisterDriver("mysql", orm.DR_MySQL)
	orm.RegisterDataBase("default", "mysql", "root:toor@/beego?charset=utf8")

	err := orm.RunSyncdb(name, force, verbose)
	if err != nil {
	    fmt.Println(err)
	}
}


func main() {
    fmt.Println("I'm up")
    beego.Run()
}
