package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type Jhm_20150122_020031 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &Jhm_20150122_020031{}
	m.Created = "20150122_020031"
	migration.Register("Jhm_20150122_020031", m)
}

// Run the migrations
func (m *Jhm_20150122_020031) Up() {
	// use m.Sql("CREATE TABLE ...") to make schema update
	m.Sql("CREATE TABLE jhm(`id` int(11) NOT NULL AUTO_INCREMENT,`h_id` varchar(128) NOT NULL,` user_id` varchar(128) NOT NULL,PRIMARY KEY (`id`))")
}

// Reverse the migrations
func (m *Jhm_20150122_020031) Down() {
	// use m.Sql("DROP TABLE ...") to reverse schema update
	m.Sql("DROP TABLE `jhm`")
}
