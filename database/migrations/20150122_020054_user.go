package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type User_20150122_020054 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &User_20150122_020054{}
	m.Created = "20150122_020054"
	migration.Register("User_20150122_020054", m)
}

// Run the migrations
func (m *User_20150122_020054) Up() {
	// use m.Sql("CREATE TABLE ...") to make schema update
	m.Sql("CREATE TABLE user(`id` int(11) NOT NULL AUTO_INCREMENT,`name` varchar(128) NOT NULL,` user_id` varchar(128) NOT NULL,` email` varchar(128) NOT NULL,` is_judge` tinyint(1) NOT NULL,` joined_on` datetime NOT NULL,` judge_invite` tinyint(1) NOT NULL,PRIMARY KEY (`id`))")
}

// Reverse the migrations
func (m *User_20150122_020054) Down() {
	// use m.Sql("DROP TABLE ...") to reverse schema update
	m.Sql("DROP TABLE `user`")
}
