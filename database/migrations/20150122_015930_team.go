package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type Team_20150122_015930 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &Team_20150122_015930{}
	m.Created = "20150122_015930"
	migration.Register("Team_20150122_015930", m)
}

// Run the migrations
func (m *Team_20150122_015930) Up() {
	// use m.Sql("CREATE TABLE ...") to make schema update
	m.Sql("CREATE TABLE team(`id` int(11) NOT NULL AUTO_INCREMENT,`name` varchar(128) NOT NULL,` team_id` varchar(128) NOT NULL,` _u_id1` varchar(128) NOT NULL,` _u_id2` varchar(128) NOT NULL,` _u_id3` varchar(128) NOT NULL,` _u_id4` varchar(128) NOT NULL,` _h_id` varchar(128) NOT NULL,` _acc_u_id1` tinyint(1) NOT NULL,` _acc_u_id2` tinyint(1) NOT NULL,` _acc_u_id3` tinyint(1) NOT NULL,` _acc_u_id4` tinyint(1) NOT NULL,` creator` varchar(128) NOT NULL,` created_at` datetime NOT NULL,PRIMARY KEY (`id`))")
}

// Reverse the migrations
func (m *Team_20150122_015930) Down() {
	// use m.Sql("DROP TABLE ...") to reverse schema update
	m.Sql("DROP TABLE `team`")
}
