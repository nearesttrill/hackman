package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type Hackathon_20150122_020015 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &Hackathon_20150122_020015{}
	m.Created = "20150122_020015"
	migration.Register("Hackathon_20150122_020015", m)
}

// Run the migrations
func (m *Hackathon_20150122_020015) Up() {
	// use m.Sql("CREATE TABLE ...") to make schema update
	m.Sql("CREATE TABLE hackathon(`id` int(11) NOT NULL AUTO_INCREMENT,`name` varchar(128) NOT NULL,` _h_id` varchar(128) NOT NULL,` created_on` datetime NOT NULL,` _ended_on` datetime NOT NULL,` start_date` datetime NOT NULL,` end_date` datetime NOT NULL,PRIMARY KEY (`id`))")
}

// Reverse the migrations
func (m *Hackathon_20150122_020015) Down() {
	// use m.Sql("DROP TABLE ...") to reverse schema update
	m.Sql("DROP TABLE `hackathon`")
}
